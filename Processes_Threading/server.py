import socket
import hashlib
import time
import math


def main_server_function(port: int = 8000):
    '''
    :param port : port number to accept the incoming requests
    :param num_of_workers : number of workers to handle the requests
    '''
    backlog = 10
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('', port))
    s.listen(backlog)
    print(f'Server running on port: {port}')
    while True:
        clientsocket, clientaddress = s.accept()
        print(f'Received a connection from {clientaddress}')
        while True:
            data = clientsocket.recv(1024).decode()
            begin_time = time.time()
            if data == 'q' or data == 'Q':
                clientsocket.close()
                print('Client quits')
                break
            else:
                # begin_time = time.time()
                print(f"Data received: {data}")
                new_num = math.factorial(int(data))
                new_data = hashlib.md5(str(new_num).encode()).hexdigest()
                spent_time = time.time() - begin_time
                print(f"Factorial: {new_num}, Data sent: {new_data}, Spent time: {spent_time}")
                clientsocket.send(new_data.encode())
