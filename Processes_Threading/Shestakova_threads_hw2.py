import threading
import time
import signal


def sig_handler(sig_num, frame):
    global is_run
    is_run = False
    print('Exit')


signal.signal(signal.SIGINT, sig_handler)

sem = threading.Semaphore()

is_run = True


def fun1():
    while is_run is True:
        sem.acquire()
        print(1)
        sem.release()
        time.sleep(0.25)


def fun2():
    while is_run is True:
        sem.acquire()
        print(2)
        sem.release()
        time.sleep(0.25)


t1 = threading.Thread(target = fun1)
t1.start()
t2 = threading.Thread(target = fun2)
t2.start()


