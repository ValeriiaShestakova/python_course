import socket
import hashlib
import time
import math
import os
import sys


def server_function(clientsocket, childpid=None):
    while True:
        data = clientsocket.recv(1024).decode()
        if data == 'q' or data == '':
            clientsocket.close()
            print('Client quits')
            break
        else:
            begin_time = time.time()
            if childpid is None:
                print(f"Main process received: {data}")
            else:
                print(f"Child {childpid} received: {data}")
            new_num = math.factorial(int(data))
            new_data = hashlib.md5(str(new_num).encode()).hexdigest()
            spent_time = time.time() - begin_time
            print(f"Data sent: {new_data}, Spent time: {spent_time}")
            clientsocket.send(new_data.encode())


def main_server_function(port: int = 8000, num_of_workers: int = 2):
    """
    :param port : port number to accept the incoming requests
    :param num_of_workers : number of workers to handle the requests
    """

    backlog = 10
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('', port))
    s.listen(backlog)
    print(f'Server running on port: {port}')

    for i in range(num_of_workers):
        pid = os.fork()
        if pid == 0:
            childpid = os.getpid()
            print(f"Child {childpid} listening on localhost:{port}")
            while True:
                clientsocket, clientaddress = s.accept()
                print(f'Received a connection from {clientaddress}')
                server_function(clientsocket, childpid)

    try:
        clientsocket, clientaddress = s.accept()
        print(f'Received a connection from {clientaddress}')
        server_function(clientsocket)
    except KeyboardInterrupt:
        print("EXIT SERVER")
        sys.exit()


main_server_function()
