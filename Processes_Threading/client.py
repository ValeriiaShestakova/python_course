import socket

port = 8000
c = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
c.connect(('', port))
while True:
    print('Write number:')
    data = input()
    c.send(data.encode())
    if data == 'q':
        c.close()
        break
    print(c.recv(1024))
