import psutil


def process_count(username: str) -> int:
    all_proc = psutil.pids()
    user_proc = list(filter(lambda p: psutil.Process(p).username() == username, all_proc))
    return len(user_proc)


def total_memory_usage(root_pid: int) -> int:
    root_p = psutil.Process(root_pid)
    all_mem = root_p.memory_full_info()[7]
    for p in root_p.children(recursive=True):
        all_mem += p.memory_full_info()[7]
    return all_mem


print(process_count('valeria'))
print(total_memory_usage(1151))
