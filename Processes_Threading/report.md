
**N clients = 3**

| Input number  | With multithreading (time) | Without multithreading (time)  |
| :------------ |:--------------------:| :----------------------:|
| 1000          | 0.0017               | 0.0023                  |
| 10000         | 0.0447               | 0.1315                  |
| 100000        | 5.6165               | 16.2954                 |




