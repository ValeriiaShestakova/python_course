glob_var1 = 0
glob_var2 = 2


def make_it_count(func, counter_name):
    def wrapper(*args, **kwargs):
        count = globals().get(counter_name)
        globals().update({counter_name: count+1})
        return func(*args, **kwargs)
    return wrapper


def avg(a, b):
    return (a+b)/2


avg = make_it_count(avg, 'glob_var1')
print(avg(2, 3))
print(avg(3, 3))
print(avg(5, 3))
print(glob_var1)
avg = make_it_count(avg, 'glob_var2')
print(avg(12, 4))
print(avg(6, 8))
print(glob_var2)
