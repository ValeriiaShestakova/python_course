def atom(val=None):
    def get_value():
        return val

    def set_value(set_v):
        nonlocal val
        val = set_v
        return val

    def process_value(*args):
        nonlocal val
        for i in args:
            val = i(val)
        return val

    return get_value, set_value, process_value


def incr(a):
    return a+1


def decr(a):
    return a-1


get_val, set_val, process_val = atom()
print(get_val())
print(set_val(2))
print(process_val(incr, incr, decr))
