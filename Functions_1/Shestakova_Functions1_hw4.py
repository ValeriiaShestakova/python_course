def let_range(start, stop, step=1):
    lst = []
    for char in range(ord(start), ord(stop), step):
        lst.append(chr(char))
    return lst


def letters_range(*args):
    start = 'a'
    step = 1
    if len(args) == 1:
        stop = args[0]
    elif len(args) == 2:
        start, stop = args
    elif len(args) == 3:
        start, stop, step = args
    else:
        return 'Ошибка ввода данных'
    return let_range(start, stop, step)


print(letters_range('b', 'w', 2))
