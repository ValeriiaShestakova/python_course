import inspect
from inspect import signature


def partial(func, *fixated_args, **fixated_kwargs):
    def wrapper(*args, **kwargs):
        if len(args) == 0 and len(kwargs) == 0:
            return func(*fixated_args, **fixated_kwargs)
        else:
            new_args = fixated_args + args
            fixated_kwargs.update(kwargs)
            return func(*new_args, **fixated_kwargs)
    wrapper.__name__ = 'partial ' + func.__name__
    try:
        sig = signature(func)
        name_fixated_args = tuple(sig.parameters.keys())[0:len(fixated_args)]
    except ValueError:
        name_fixated_args = []
    positional_dict = {}
    positional_args = ''
    for i in range(len(fixated_args)):
        positional_dict.update({name_fixated_args[i]: fixated_args[i]})
    for key, value in positional_dict.items():
        positional_args += f'{key} = {value}, '
    key_args = ''
    for key, value in fixated_kwargs.items():
        key_args += f'{key} = {value}, '
    wrapper.__doc__ = inspect.cleandoc(f"""
        A partial implementation of {func.__name__}
        with pre-applied arguements being:  
        {positional_args}{key_args}
        """)[:-2:]
    return wrapper


def some_fun(a, b, x=5, y=5):
    return x*(a+b)/2 - y


some_fun = partial(some_fun, 5, 6, x=3, y=1)
print(some_fun(x=4, y=4))
print(some_fun.__doc__)