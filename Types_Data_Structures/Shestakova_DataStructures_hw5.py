n = 1000000
sum = 0
for i in range(1, n):
    if str(i) == str(i)[::-1] and bin(i)[2::] == bin(i)[:1:-1]:
        sum += i

print(sum)


