print('Введите текст')
while True:
    txt = input()
    if txt == 'cancel':
        print('Bye!')
        break
    else:
        new_txt = txt.lower().split()
        txt_dict = {}
        for item in new_txt:
            txt_dict.update({item: new_txt.count(item)})
        max_val = 0
        for value in txt_dict.values():
            if max_val < value:
                max_val = value
        for key, value in txt_dict.items():
            if value == max_val:
                print('{} - {}'.format(value, key))
