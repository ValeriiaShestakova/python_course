
# Вариант без учета неотрицательных чисел

# print('Введите целые неотрицательные числа, разделенные любым не цифровым литералом (пробел, запятая, %, буква и т.д.)')
# while True:
#     txt = input()
#     if txt == 'cancel':
#         print('Bye!')
#         break
#     else:
#         i = 0
#         sum = 0
#         while i < len(txt):
#             if txt[i].isdigit():
#                 num = ''
#                 while txt[i].isdigit():
#                     num += txt[i]
#                     i += 1
#                     if i == len(txt):
#                         break
#                 sum += int(num)
#             else:
#                 i += 1
#         print(sum)


# Вариант с учетом отрицательных чисел
print('Введите целые числа, разделенные любым не цифровым литералом (пробел, запятая, %, буква и т.д.)')
while True:
    txt = input()
    if txt == 'cancel':
        print('Bye!')
        break
    else:
        i = 0
        sum = 0
        while i < len(txt):
            if txt[i].isdigit():
                if i == 0 or txt[i-1] != '-':
                    factor = 1
                else:
                    factor = -1
                num = ''
                while txt[i].isdigit():
                    num += txt[i]
                    i += 1
                    if i == len(txt):
                        break
                sum += factor*int(num)
            else:
                i += 1
        print(sum)
