print('Введите неотрицательные целые числа, разделенные через пробел')
num_lst = input().split()
max_n = 0
for item in num_lst:
    if max_n < int(item):
        max_n = int(item)

# Вычислительная сложность данного алгоритма O(n^2), так как два вложенных цикла
for i in range(1, max_n+2):
    flag = True
    for item in num_lst:
        if i == int(item):
            flag = False
    if flag:
        print(i)
        break

