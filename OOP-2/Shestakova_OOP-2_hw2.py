class EnumMeta(type):
    enum_storage = {}

    def __new__(mcs, name, bases, dct):
        for attr_name, value in dct.items():
            if not attr_name.startswith('_'):
                mcs.enum_storage[attr_name] = value
        enum_class = super().__new__(mcs, name, bases, dct)
        enum_class.instance_store = []
        for key, value in mcs.enum_storage.items():
            enum_member = enum_class(value)
            setattr(enum_class, key, enum_member)
            enum_class.instance_store.append((key, enum_member))
        return enum_class

    def __iter__(self):
        return iter(self.enum_storage)

    def __getitem__(self, item):
        return self.enum_storage[item]


class Enum(metaclass=EnumMeta):

    def __new__(cls, value):
        inv_dct = {value: key for key, value in EnumMeta.enum_storage.items()}
        if value not in EnumMeta.enum_storage.values():
            raise ValueError
        for item in cls.instance_store:
            if item[0] == inv_dct[value]:
                return item[1]
        enum_member = super().__new__(cls)
        enum_member.name = inv_dct[value]
        enum_member.value = value
        enum_member.__class__ = cls
        return enum_member


class Direction(Enum):
    north = 0
    east = 90
    south = 180
    west = 270


print(Direction.east.value)
for d in Direction:
    print(d)

print(id(Direction.north))  # 2171479819208
print(id(Direction(0)))  # 2171479819208

print(Direction['west'])



