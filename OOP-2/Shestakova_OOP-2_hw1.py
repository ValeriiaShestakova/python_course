class Descriptor:

    def __init__(self, var_name='var'):
        self.var_name = var_name

    def __get__(self, instance, owner):
        return getattr(instance, self.var_name, owner)

    def __set__(self, instance, value):
        try:
            if value >= 0:
                print(f'Setting number {value}')
                setattr(instance, self.var_name, value)
            else:
                print("Number must be positive!")
        except TypeError:
            print('Enter the number!')


class BePositive:
    some_value = Descriptor('some')
    another_value = Descriptor('another')


inst = BePositive()
inst2 = BePositive()

inst.some_value = 5
inst.another_value = -2
inst2.some_value = 3
inst2.another_value = -8
