from Shestakova_OOP_hw1 import Calculator


class AbstractOptimiser:
    def process(self, input_data, flag):
        if flag is False:
            postfix_exp = self.pre_process(input_data)
        else:
             postfix_exp = input_data
        changing, flag = self.process_internal(postfix_exp)
        return self.post_process(changing, flag)

    def pre_process(self, input_data):
        postfix_exp = Calculator(input_data).to_opn()
        return postfix_exp

    def process_internal(self, data):
        return data, False

    def post_process(self, result, flag=False):
        c = Calculator(result)
        return c.opcodes, flag


class DoubleNegativeOptimiser(AbstractOptimiser):
    def process_internal(self, input_data):
        tokens = 'abcdefghijklmnopqrstuvwxyz'
        flag = True
        queue = []
        i = 0
        count = 0
        while i < len(input_data):
            while i < len(input_data) and input_data[i] == '-':
                count += 1
                i += 1
            if count == 1:
                queue.append('-')
            if count >= 2:
                if len(queue) > 1:
                    # if input_data[i-count-2] in tokens or input_data[i-count-2].isdigit():
                    #     for c in range(count):
                    #         queue.append('-')
                    # else:
                    if count % 2 == 0:
                        queue.append('+')
                    else:
                        queue.append('-')
            else:
                if i < len(input_data):
                    queue.append(input_data[i])
                i += 1
            count = 0
        return queue, flag


class IntegerCostantsOptimiser(AbstractOptimiser):

    def choose_operations(self, first, second, operation):
        if operation == '+':
            return [str(first + second)]
        if operation == '-':
            if first - second >= 0:
                return [str(first - second)]
            else:
                return [str(second - first)] + ['-']
        if operation == '*':
            return [str(first * second)]
        if operation == '/':
            return [str(round(first / second))]
        if operation == '^':
            return [str(first**second)]

    def check(self, queue, flag):
        for i, val in enumerate(queue):
            if i < len(queue)-1 and val.isdigit() and queue[i+1].isdigit():
                return self.process_internal(queue)
        return queue, flag

    def process_internal(self, input_data):
        flag = True
        queue = []
        i = 0
        if len(input_data) < 3:
            return input_data, flag
        while i < len(input_data):
            if input_data[i].isdigit():
                if input_data[i+1].isdigit():
                    queue.extend(self.choose_operations(int(input_data[i]),
                                                        int(input_data[i+1]),
                                                        input_data[i+2]))
                    i += 3
            if i < len(input_data):
                queue.append(input_data[i])
            i += 1
        queue, flag = self.check(queue, flag)
        return queue, flag


class SimplifierOptimiser(AbstractOptimiser):
    def same_symbols(self, input_data):
        tokens = 'abcdefghijklmnopqrstuvwxyz'
        i = 0
        queue = []
        while i < len(input_data):
            if i < len(input_data)-2 and input_data[i] in tokens and \
                    input_data[i+1] == input_data[i]:
                if input_data[i+2] == '-':
                    queue.append('0')
                elif input_data[i+2] == '/':
                    queue.append('1')
                i += 3
            elif input_data[i] == '1':
                if 0 < i and input_data[i + 1] == '*' and (input_data[i - 1] in tokens):
                    queue.pop()
                    queue.append(input_data[i-1])
                    i += 2
                elif i < len(input_data) - 2 and input_data[i + 2] == '*' and (
                        input_data[i + 1] in tokens):
                    queue.append(input_data[i+1])
                    i += 3
                else:
                    queue.append(input_data[i])
                    i += 1
            else:
                queue.append(input_data[i])
                i += 1
        return queue

    def find_zero_operations(self, input_data):
        tokens = 'abcdefghijklmnopqrstuvwxyz'
        i = 0
        queue = []
        while i < len(input_data):
            if input_data[i] == '0':
                if (i > 0 and input_data[i+1] == '+' or input_data[i+1] == '-')\
                        and (input_data[i-1] in tokens):
                    queue.pop()
                    queue.append(input_data[i-1])
                    i += 2
                elif i < len(input_data)-2 and (input_data[i+2] == '+' or input_data[i+2] == '-')\
                        and (input_data[i+1] in tokens):
                    queue.append(input_data[i+1])
                    if input_data[i+2] == '-':
                        queue.append('-')
                    i += 3
                elif 0 < i and input_data[i+1] == '*' and (input_data[i-1] in tokens):
                    queue.pop()
                    queue.append('0')
                    i += 2
                elif i < len(input_data)-2 and input_data[i+2] == '*' and (input_data[i+1] in tokens):
                    queue.append('0')
                    i += 3
                elif i < len(input_data)-2 and input_data[i+2] == '/' and (input_data[i+1] in tokens):
                    queue.append('0')
                    i += 3
                elif input_data[i+1] == '^' and (input_data[i-1] in tokens):
                    queue.pop()
                    queue.append('1')
                    i += 2
                elif input_data[i+2] == '^' and (input_data[i+1] in tokens):
                    queue.append('0')
                    i += 3
            else:
                if i < len(input_data):
                    queue.append(input_data[i])
                i += 1
        return queue

    def process_internal(self, input_data):
        flag = True
        if len(input_data) < 3:
            return input_data, flag
        queue = input_data
        queue = self.same_symbols(queue)
        while '0' in queue and len(queue) > 1:
            queue = self.find_zero_operations(queue)
        return queue, flag




