class Calculator:
    def __init__(self, opcodes: list, operators=None):
        self.opcodes = opcodes
        self.flag = False
        self.operators = operators if operators is not None else []

    def to_opn(self):
        """
        Method for converting input data to reverse Polish notation
        :return: <str> Converting data or error
        """
        tokens = 'abcdefghijklmnopqrstuvwxyz'
        priority = {'^': 4, '*': 3, '/': 3, '+': 2, '-': 2, '(': 1}
        operators = ['+', '^', '*', '/']
        stack = []
        output_data = []
        last_token = len(self.opcodes)-1
        for idx, val in enumerate(self.opcodes):
            if idx == 0 and val in operators:
                return "error"
            if len(self.opcodes) != idx + 1:
                if val == '(' and self.opcodes[idx+1] in operators:
                    return "error"
                elif val == ')' and (self.opcodes[idx-1] in operators
                                     or self.opcodes[idx-1] == '-'):
                    return "error"
            if val in tokens or val.isdigit():
                last_token = idx
        for idx, i in enumerate(self.opcodes):
            if len(self.opcodes) != idx + 1 and \
                    (priority.get(i) and self.opcodes[idx+1]) in operators:
                return "error"
            if idx == last_token:
                if len(self.opcodes) != idx + 1 and self.opcodes[idx+1] == ')':
                    last_token = idx+1
                elif len(stack) != 0 and len(self.opcodes)-1 > idx:
                    return "error"
            if i in tokens or i.isdigit():
                output_data.append(i)
            elif i == '(':
                stack.append(i)
            elif i == ')':
                while priority.get(stack[len(stack) - 1]) != 1:
                    output_data.append(stack.pop())
                stack.pop()
            elif len(stack) == 0 or priority.get(i) > priority.get(
                    stack[len(stack) - 1]):
                stack.append(i)
            else:
                while priority.get(i) <= priority.get(stack[len(stack) - 1]):
                    output_data.append(stack.pop())
                    if len(stack) == 0:
                        break
                stack.append(i)
        while len(stack) != 0:
            output_data.append(stack.pop())
        if ('(' or ')') in output_data:
            return "error"
        return output_data

    def __str__(self) -> str:
        if self.flag is False:
            return ' '.join(self.to_opn())
        else:
            return ' '.join(self.opcodes)

    def optimise(self):
        for operator in self.operators:
            self.opcodes, self.flag = operator.process(self.opcodes, self.flag)
        return self.opcodes

    @staticmethod
    def check_division(input_data):
        """
        Method for check division on zero
        :param input_data: <list> Expression for check
        :return: <boolean> True if no divisions in expression
        """
        for idx, val in enumerate(input_data):
            if val == '/':
                if idx < len(input_data)-1:
                    if input_data[idx+1] == '0':
                        return False
        return True

    def validate(self) -> bool:
        if self.to_opn() == 'error' or \
                not self.check_division(self.opcodes):
            return False
        else:
            return True

