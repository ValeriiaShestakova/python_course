import os


def hardlink_check(directory_path: str) -> bool:
    try:
        files = os.listdir(directory_path)
    except FileNotFoundError or NotADirectoryError:
        return False
    inodes = set()
    for f in files:
        f_path = os.path.join(directory_path, f)
        if os.stat(f_path).st_ino not in inodes:
            inodes.add(os.stat(f_path).st_ino)
        else:
            return True
    return False


print(hardlink_check('dir2'))
