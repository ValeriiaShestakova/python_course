import tempfile


def merge_files(fname1: str, fname2: str) -> str:
    f1 = open(fname1)
    f2 = open(fname2)
    line1 = f1.readline().strip()
    line2 = f2.readline().strip()
    tmp = tempfile.TemporaryFile(dir='temp', delete=False)
    while line1 or line2:
        if line1 == '':
            while line2:
                tmp.write((line2+'\n').encode())
                line2 = f2.readline().strip()
            break
        elif line2 == '':
            while line1:
                tmp.write((line1+'\n').encode())
                line1 = f1.readline().strip()
            break
        if int(line1) < int(line2):
            tmp.write((line1+'\n').encode())
            line1 = f1.readline().strip()
        elif int(line2) <= int(line1):
            tmp.write((line2+'\n').encode())
            line2 = f2.readline().strip()
    f1.close()
    f2.close()
    tmp.close()
    return tmp.name


print(merge_files('f1.txt', 'f2.txt'))

