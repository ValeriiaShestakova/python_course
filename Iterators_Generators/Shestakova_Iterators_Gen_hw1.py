class Graph:
    def __init__(self, E):
        self.E = E


class GraphIterator:
    def __iter__(self):
        return self

    def __init__(self, graph, start_v):
        self.graph = graph
        self.start_v = start_v
        self.included_v = []
        self.queue = [start_v]

    def hasNext(self) -> bool:
        if len(self.included_v) < len(graph.E.keys()):
            return True
        else:
            return False

    def __next__(self) -> str:
        if self.hasNext():
            cur_v = ''
            if len(self.queue) == 0:
                for i in graph.E.keys():
                    if i not in self.included_v:
                        cur_v = i
                        break
            else:
                cur_v = self.queue.pop(0)
            if cur_v not in self.included_v:
                self.included_v.append(cur_v)
                for item in graph.E.get(cur_v):
                    if item not in self.included_v:
                        self.queue.append(item)
                return cur_v
            else:
                return self.__next__()
        else:
            raise StopIteration


graph = Graph({"A": ["C", "E"], "B": ["C", "E", "D"],
               "C": ["A", "B", "E"], "D": ["B", "E"],
               "E": ["A", "B", "C", "D"], "F": ["H"], "H": ["F"]})

start_v = 'A'
my_iter = GraphIterator(graph, start_v)


for i in my_iter:
    print(i)

# print(next(my_iter))
# print(next(my_iter))
# print(next(my_iter))
# print(next(my_iter))
# print(next(my_iter))
