tokens = 'abcdefghijklmnopqrstuvwxyz'
priority = {'*': 3, '/': 3, '+': 2, '-': 2, '(': 1}


def to_opn(input_data):
    stack = []
    output_data = ''
    for i in input_data:
        if i in tokens:
            output_data += i
        elif i == '(':
            stack.append(i)
        elif i == ')':
            while priority.get(stack[len(stack) - 1]) != 1:
                output_data += stack.pop()
            stack.pop()
        elif len(stack) == 0 or priority.get(i) > priority.get(
                stack[len(stack) - 1]):
            stack.append(i)
        else:
            while priority.get(i) <= priority.get(stack[len(stack)-1]):
                output_data += stack.pop()
                if len(stack) == 0:
                    break
            stack.append(i)
    while len(stack) != 0:
        output_data += stack.pop()
    return output_data


def from_opn(input_data):
    stack = []
    for i in input_data:
        if i in tokens:
            stack.append([i, 3])
        else:
            if priority.get(i) == 2:
                el1, el2 = stack.pop(), stack.pop()
                if i == '-':
                    if el1[1] == 2:
                        stack.append([el2[0] + i + '(' + el1[0] + ')', 2])
                    else:
                        stack.append([el2[0] + i + el1[0], 2])
                else:
                    stack.append([el2[0]+i+el1[0], 2])
            else:
                el1, el2 = stack.pop(), stack.pop()
                pr1 = el1[1]
                pr2 = el2[1]
                if priority.get(i) > pr2 and priority.get(i) > pr1:
                    stack.append(['(' + el2[0] + ')' + i + '(' + el1[0] + ')', 3])
                elif priority.get(i) > pr2:
                    if i == '/':
                        if el1[1] == 3 and el1[0] not in tokens:
                            stack.append(['(' + el2[0] + ')' + i + '(' + el1[0] + ')', 3])
                        elif el1[1] == 3 and el1[0] in tokens:
                            stack.append(['(' + el2[0] + ')' + i + el1[0], 3])
                    else:
                        stack.append(['(' + el2[0] + ')' + i + el1[0], 3])
                elif priority.get(i) > pr1:
                    stack.append([el2[0] + i + '(' + el1[0] + ')', 3])
                else:
                    if i == '/':
                        if el1[1] == 3 and el1[0] not in tokens:
                            stack.append([el2[0] + i + '(' + el1[0] + ')', 3])
                        elif el1[1] == 3 and el1[0] in tokens:
                            stack.append([el2[0] + i + el1[0], 3])
                    else:
                        stack.append([el2[0] + i + el1[0], 3])
    return stack[0][0]


def brackets_trim(input_data: str) -> str:
    res = to_opn(input_data)
    return from_opn(res)


input_data = '(a*(b/c)+((d-f)/k))*(h*(g-r))'
print(brackets_trim(input_data))
