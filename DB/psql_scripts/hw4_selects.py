import psycopg2
import json
import time


with psycopg2.connect(host='172.18.0.2', port='5432', user='postgres', dbname='my_db') as conn:

    cursor = conn.cursor()
    cursor.execute("""SELECT first_name, last_name FROM employee GROUP BY first_name, last_name
                   HAVING COUNT(*) = (SELECT MAX(col) FROM
                    (SELECT first_name, last_name, COUNT(*) AS col FROM employee GROUP BY first_name, last_name ) as T)""")
    results = [' '.join(r) for r in cursor.fetchall()]
    hw1_str = ', '.join(results)
    answer_dict = {'hw1': hw1_str}

    cursor.execute("""SELECT COUNT(*) FROM employee WHERE employee_city != (SELECT UPPER(department_city) FROM department
        WHERE department_name = employee_department)""")
    res = cursor.fetchone()[0]
    answer_dict['hw2'] = res

    cursor.execute("""WITH em AS (SELECT * FROM employee),
    b AS (SELECT * FROM employee WHERE employee_id IN (SELECT boss FROM employee))
    SELECT COUNT(*) FROM em, b WHERE em.boss = b.employee_id AND em.salary > b.salary""")

    res = cursor.fetchone()[0]
    answer_dict['hw3'] = res

    begin_time = time.time()
    cursor.execute("""WITH sal AS (SELECT employee_department, avg(salary) as s FROM employee GROUP BY employee_department)
        SELECT employee_department FROM sal WHERE s = (SELECT max(s) FROM sal)
    """)
    results = [' '.join(r) for r in cursor.fetchall()]
    hw4_str = ', '.join(results)
    answer_dict['hw4'] = hw4_str

    cursor.execute("""SELECT department_name FROM department""")
    deps = cursor.fetchall()
    result = {}
    for d in deps:
        cursor.execute("""WITH rnk AS (SELECT employee_department AS dep, salary AS s, rank()
        OVER (PARTITION BY employee_department ORDER BY salary DESC) AS r FROM employee),
        dep_count AS (SELECT  dep as d, COUNT(*) as c FROM rnk GROUP BY dep),
        max_s AS (SELECT SUM(s) from rnk, dep_count WHERE dep = %s and r <= 0.1*dep_count.c),
        min_s AS (SELECT SUM(s) from rnk, dep_count WHERE dep = %s and r >= 0.9*dep_count.c)
        SELECT * FROM max_s, min_s
           """, (d, d))
        res = cursor.fetchone()
        result[d[0]] = res[0]/res[1]

    res = []
    sorted_r = sorted(result.items(), key=lambda item: -item[1])
    new_r = sorted_r[:2:]
    for i in new_r:
        res.append(i[0])
    answer_dict['hw5'] = res

    answer_json = json.dumps(answer_dict, indent=4)
    print(answer_json)
