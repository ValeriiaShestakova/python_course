import psycopg2

with psycopg2.connect(host='172.18.0.2', port='5432', user='postgres', dbname='my_db') as conn:

    cursor = conn.cursor()

    cursor.execute("""
        COPY department(department_name, department_city) FROM '/home/csv_data/DEPTS.csv' WITH (FORMAT csv, HEADER true);
        ALTER TABLE employee DROP  employee_department;
        ALTER TABLE employee ADD COLUMN employee_department VARCHAR(255);
        ALTER TABLE department ALTER COLUMN department_name SET NOT NULL;
        ALTER TABLE department ADD CONSTRAINT dep_key UNIQUE (department_name);
        ALTER TABLE employee ADD CONSTRAINT employee_department FOREIGN KEY (employee_department) REFERENCES department (department_name);
        COPY employee(employee_id, first_name, last_name, employee_department, employee_city, boss, salary) FROM '/home/csv_data/EMPLOYEE.csv' WITH (FORMAT csv, HEADER true);
    """)
