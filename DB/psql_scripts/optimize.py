import psycopg2


with psycopg2.connect(host='172.18.0.2', port='5432', user='postgres', dbname='my_db') as conn:

    cursor = conn.cursor()
    cursor.execute("""CREATE INDEX dep_city_ind ON department(upper(department_city))""")
    cursor.execute("""CREATE INDEX sal_ind ON employee(salary)""")
    cursor.execute("""CREATE INDEX boss_ind ON employee(boss)""")
