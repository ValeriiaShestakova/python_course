﻿import unittest
import time
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, NoSuchWindowException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains


class AutoLike(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()

    def login_to_tinder(self):
        driver = self.driver
        driver.maximize_window()
        driver.find_element_by_xpath(
            '//*[@id="modal-manager"]/div/div/div[2]/div/div[3]/div[1]/button').click()
        driver.switch_to.window(driver.window_handles[1])
        self.assertIn("Facebook", driver.title)
        # Type your email on Facebook
        input_email = driver.find_element_by_id('email')
        input_email.send_keys('')
        # Type your password on Facebook
        input_pass = driver.find_element_by_name('')
        input_pass.send_keys('silwerhof')
        driver.find_element_by_id('loginbutton').click()
        try:
            driver.find_element_by_id('error_box')
            print('Неверно введены логин или пароль')
            self.tearDown()
        except NoSuchWindowException:
            pass
        driver.switch_to.window(driver.window_handles[0])
        self.assertIn("Tinder", driver.title)
        driver.implicitly_wait(3)
        # Skipping tips
        try:
            # Добро пожаловать
            driver.find_element_by_xpath('//*[@id="content"]/div/span/div/div[2]/div/div[1]/div[1]/div/div[3]/button').click()
            time.sleep(2)
            # Удобство общения
            driver.find_element_by_xpath('//*[@id="content"]/div/span/div/div[2]/div/div/main/div/div[3]/button').click()
            time.sleep(2)
            # Местоположение
            driver.find_element_by_xpath('//*[@id="content"]/div/span/div/div[2]/div/div/div[1]/div/div[3]/button[1]').click()
            time.sleep(2)
            # Уведомления
            driver.find_element_by_xpath('//*[@id="content"]/div/span/div/div[2]/div/div/div[1]/div/div[3]/button[2]').click()
        except NoSuchElementException:
            # Местоположение
            driver.find_element_by_xpath(
                '//*[@id="content"]/div/span/div/div[2]/div/div/div[1]/div/div[3]/button[1]').click()
            time.sleep(2)
            # Уведомления
            driver.find_element_by_xpath(
                '//*[@id="content"]/div/span/div/div[2]/div/div/div[1]/div/div[3]/button[2]').click()
            time.sleep(2)
            # Добро пожаловать
            driver.find_element_by_xpath(
                '//*[@id="content"]/div/span/div/div[2]/div/div[1]/div[1]/div/div[3]/button').click()
            time.sleep(2)
            # Удобство общения
            driver.find_element_by_xpath(
                '//*[@id="content"]/div/span/div/div[2]/div/div/main/div/div[3]/button').click()

    def like_people(self):
        driver = self.driver
        actions = ActionChains(driver)
        time.sleep(2)
        while True:
            try:
                cont = driver.find_element_by_xpath('//*[@id="content"]/div/span/div/div[1]/div/main/div[1]/div/div/div[1]/div[2]/div[2]/div[1]/div/button')
                cont.click()
            except NoSuchElementException:
                pass
            actions.send_keys(Keys.RIGHT).perform()
            time.sleep(1)

    def test_like(self):
        driver = self.driver
        driver.get("https://tinder.com/app/recs")
        self.assertIn("Tinder", driver.title)
        driver.implicitly_wait(15)
        self.login_to_tinder()
        self.like_people()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()



