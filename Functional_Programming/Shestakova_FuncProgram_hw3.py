def collatz_steps(n):
    i = 0
    if n <= 0:
        return 'Введите натуральное число!'
    # Function for converting even and odd numbers, x - number, i - steps
    number_conversion = lambda x, i: cycle_condition(round(x / 2), i + 1) \
        if x % 2 == 0 else cycle_condition((3 * x + 1), i + 1)
    # Function for performing conversion and to stop a cycle, n - number, i - steps
    cycle_condition = lambda n, i: number_conversion(n, i) if n > 1 else i
    return cycle_condition(n, i)


assert collatz_steps(16) == 4
assert collatz_steps(12) == 9
assert collatz_steps(1000000) == 152