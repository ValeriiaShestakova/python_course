from functools import reduce


def is_armstrong(number):
    n = len(str(number))
    # lst = map(lambda x: int(x)**n, str(number))
    lst = [int(x)**n for x in str(number)]
    sum = reduce(lambda x, y: x + y, lst)
    if sum == number:
        return True
    else:
        return False


assert is_armstrong(153) == True, 'Число Армстронга'
assert is_armstrong(10) == False, 'Не число Армстронга'

