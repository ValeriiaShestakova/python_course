import math
from functools import reduce

# First way
n = 1000
full_sqrt = map(lambda x: x**2, range(1, n))
sum_sqrt = reduce(lambda x, y: x+y, full_sqrt)
print('First way:')
print(sum_sqrt)


# Second way
n = 1000

full_sqr = sum([x**2 for x in range(1, n)])
print('Second way:')
print(full_sqr)


# Third way
n = 1000


def get_full_sqr(n):
    full_sqr = []
    for x in range(1, n):
        full_sqr.append(x**2)
    return full_sqr


def get_sum(lst):
    sum = 0
    for i in lst:
        sum += i
    return sum


full_sqr = get_full_sqr(n)
print('Third way:')
print(get_sum(full_sqr))

