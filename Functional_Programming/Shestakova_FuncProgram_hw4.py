from functools import reduce

problem9 = [(a, b, 1000-a-b) for a in range(1, 1000)
            for b in range(a+1, 1000-a) if a**2+b**2 == (1000-a-b)**2]
print(problem9[0])

problem6 = [sum(range(1, 1001))**2 - sum(x**2 for x in range(1, 1001))]
print(problem6)

problem48 = (sum(x**x for x in range(1, 1001)) % 10**10)
print(problem48)

lst = [d for i in range(200000) for d in str(i)]
problem40 = reduce(lambda x, y: int(x)*int(y), [lst[10**i] for i in range(7)])
print(problem40)

