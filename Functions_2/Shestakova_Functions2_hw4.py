import time


def make_cache(save_time):
    def cache(func):
        storage = {}

        def wrapper(*args):
            if args in storage:
                if time.time() > storage[args][1]:
                    storage.pop(args)
            if args in storage:
                return storage[args][0]
            else:
                val = func(*args)
                storage[args] = (val, time.time()+save_time)
                return val
        return wrapper
    return cache


@make_cache(5)
def slow_function(*args):
    time.sleep(5)
    return args


print(slow_function(6))
time.sleep(10)
print(slow_function(6))
print(slow_function(6))
