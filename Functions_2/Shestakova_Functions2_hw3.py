def common_divisor(a, b):
    x = abs(b[0] - a[0])
    y = abs(b[1] - a[1])
    if x == 0:
        res = y
    elif y == 0:
        res = x
    else:
        res = max([i for i in range(1, x + 1) if
                   (x % i == 0) and (y % i == 0)])
    return res


def border_points(a, b, c):
    count1 = common_divisor(a, b) + 1
    count2 = common_divisor(b, c)
    count3 = common_divisor(a, c) - 1
    return count1 + count2 + count3


def count_points(a, b, c):
    s = abs(
        0.5 * ((a[0] - c[0]) * (b[1] - c[1]) - (b[0] - c[0]) * (a[1] - c[1])))
    bord_p = border_points(a, b, c)
    points_inside = s - 0.5*bord_p + 1
    return points_inside + bord_p


print(count_points((4, 3), (0, 4), (-2, -2)))

