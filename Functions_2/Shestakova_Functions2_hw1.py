def str_to_num(s, n=0, sum=0):
    if n == len(s):
        return get_num(sum)
    sum += (ord(s[n]) - 48) * (10**(len(s)-n-1))
    if ord(s[n]) < 48 or ord(s[n]) > 57:
        return "Не удалось преобразовать введенный текст в число."
    return str_to_num(s, n+1, sum)


def get_num(n):
    if n % 2 == 0:
        return int(n/2)
    else:
        return 3*n+1


print('Введите текст')


while True:
    txt = input()
    if txt == 'cancel':
        print('Bye!')
        break
    else:
        n = str_to_num(txt)
        print(n)
