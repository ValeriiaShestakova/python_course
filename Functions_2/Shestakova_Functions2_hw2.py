def my_sqrt(a, acc=0.1, n=None, cur_n=1):
    if n is None:
        n = 1
    if abs(cur_n**2 - a)/a >= acc:
        return my_sqrt(a, acc, n+1, 0.5*(cur_n+a/cur_n))
    else:
        n_digits = 0
        while acc < 1:
            acc *= 10
            n_digits += 1
        answer = round(cur_n, n_digits)
        while answer == 0:
            n_digits += 1
            answer = round(cur_n, n_digits)
        return answer


print(my_sqrt(5/10000000000, 0.001))

print(my_sqrt(5/100000000, 0.00001))
