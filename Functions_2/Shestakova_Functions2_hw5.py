import time
import math

var_rec = {'count': 0, 'exec_time': 0, 'n': 0, 'flag': False}
var_cache = {'count': 0, 'exec_time': 0, 'n': 0, 'flag': False}
var_sec = {'count': 0, 'exec_time': 0, 'n': 0, 'flag': False}
var_dyn = {'count': 0, 'exec_time': 0, 'n': 0, 'flag': False}
var_mat = {'count': 0, 'exec_time': 0, 'n': 0, 'flag': False}


def make_cache(function):
    cache = {}

    def wrapper(*args):
        if args in cache:
            return cache[args]
        else:
            val = function(*args)
            cache[args] = val
            return val
    return wrapper


def time_check(global_var):
    def check(func):

        def wrapper(*args):
            if globals()[global_var]['flag']:
                pass
            else:
                if args == globals()[global_var]['n']:
                    pass
                else:
                    globals()[global_var]['exec_time'] = 0
                    globals()[global_var]['count'] = 0
                globals()[global_var]['n'] = args
                globals()[global_var]['flag'] = True
            begin_time = time.time()
            val = func(*args)
            end_time = time.time()
            try:
                globals()[global_var]['exec_time'] += end_time - begin_time
                globals()[global_var]['count'] += 1
                if globals()[global_var]['n'] == args:
                    globals()[global_var]['flag'] = False
            except KeyError:
                globals()[global_var]['exec_time'] = 0
                globals()[global_var]['count'] = 0
                globals()[global_var]['n'] = 0
            return val
        return wrapper
    return check


@time_check('var_rec')
def fib_rec(n):
    if n in (1, 2):
        return 1
    return fib_rec(n-1) + fib_rec(n-2)


@make_cache
@time_check('var_cache')
def fib_cache(n):
    if n in (1, 2):
        return 1
    return fib_cache(n-1) + fib_cache(n-2)


@time_check('var_sec')
def fib_sec(n):
    sqrt5 = math.sqrt(5)
    phi = (sqrt5 + 1) / 2
    return int(phi ** n / sqrt5 + 0.5)


@time_check('var_dyn')
def fib_dyn(n):
    a = 0
    b = 1
    for __ in range(n):
        a, b = b, a + b
    return a


def pow(x, n, I, mult):
    if n == 0:
        return I
    elif n == 1:
        return x
    else:
        y = pow(x, n // 2, I, mult)
        y = mult(y, y)
        if n % 2:
            y = mult(x, y)
        return y


def identity_matrix(n):
    r = list(range(n))
    return [[1 if i == j else 0 for i in r] for j in r]


def matrix_multiply(A, B):
    BT = list(zip(*B))
    return [[sum(a * b
                 for a, b in zip(row_a, col_b))
            for col_b in BT]
            for row_a in A]


@time_check('var_mat')
def fib_mat(n):
    F = pow([[1, 1], [1, 0]], n, identity_matrix(2), matrix_multiply)
    return F[0][1]


# Методы с рекурсией уступают остальным методам даже при небольших значениях
print('Рекурсия: ')
print(fib_rec(10))
print(var_rec)
print(fib_rec(10))
print(var_rec)
print(fib_rec(25))
print(var_rec)

print('Рекурсия с кешем: ')
print(fib_cache(200))
print(var_cache)

# При n ~> 1400 функция с золотым сечением перестает работать

print('Золотое сечение: ')
print(fib_sec(1400))
print(var_sec)
#
# При большом n видно, что функция, использующая матрицы, считает быстрее

n = 10 ** 6
print('Динамическое программирование: ')
print(fib_dyn(n))
print(var_dyn)

print('С помощью матриц: ')
print(fib_mat(n))
print(var_mat)


