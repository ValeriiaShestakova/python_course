import os


def check_process_exists(pid):
    try:
        os.kill(int(pid), 0)
    except OSError:
        return False
    else:
        return True


class pidfile:
    def __init__(self, file_name: str):
        self.file_name = file_name
        cur_pid = str(os.getpid())
        if os.path.exists(file_name) is True:
            f = open(self.file_name)
            f.close()
            f = open(self.file_name)
            prev_process_id = f.read()
            if check_process_exists(prev_process_id) is True:
                print('Program is running, try later')
                raise Exception
        f = open(file_name, 'w')
        f.write(cur_pid)
        f.flush()

    def __enter__(self):

        return self.file_name

    def __exit__(self, *exc_info):
        f = open(self.file_name)
        f.close()
        os.unlink(self.file_name)


def run_with_pidfile(file_name):
    def run(func):
        def wrapper(*args, **kwargs):
            with pidfile(file_name):
                ans = func(*args, **kwargs)
            return ans
        return wrapper
    return run


@run_with_pidfile('file')
def test(a, b):
    print('Hello')
    return a/b




