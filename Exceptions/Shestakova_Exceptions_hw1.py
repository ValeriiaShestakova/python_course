import sys


class ContextManager:
    def __init__(self, _file):
        self.f = _file

    def __enter__(self):
        if self.f is not None:
            file = open(self.f, 'w')
            sys.stderr = file
        else:
            sys.stderr = sys.stdout
        return self.f

    def __exit__(self, *exc_info):
        if self.f is not None:
            file = open(self.f, 'w')
            sys.stderr = file
        else:
            sys.stderr = sys.stdout
        if self.f is not None:
            file = open(self.f)
            file.close()


def stderr_redirect(dest=None):
    def redirect(func):
        def wrapper(*args, **kwargs):
            with ContextManager(dest):
                ans = func(*args, **kwargs)
            return ans
        return wrapper
    return redirect


@stderr_redirect('err.txt')
def test(a, b):
    print('Hello')
    return a/b

