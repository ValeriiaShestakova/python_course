from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.datasets import fetch_20newsgroups
from sklearn.pipeline import Pipeline
from sklearn.naive_bayes import MultinomialNB
from sklearn import metrics
import spacy

categories = ['sci.med', 'soc.religion.christian']

twenty_train = fetch_20newsgroups(subset='train', categories=categories, shuffle=True, random_state=42)
twenty_test = fetch_20newsgroups(subset='test', categories=categories, shuffle=True, random_state=42)
docs_test = twenty_test.data

count_vectorizer = CountVectorizer()
text_clf = Pipeline([('vect', count_vectorizer),
    ('tfidf', TfidfTransformer()),
    ('clf', MultinomialNB()),
])

text_clf.fit(twenty_train.data, twenty_train.target)
predicted = text_clf.predict(docs_test)

print(metrics.classification_report(twenty_test.target, predicted,
    target_names=twenty_test.target_names))


nlp = spacy.load('en')
new_data = [nlp(x) for x in twenty_train.data]
new_data_lem = []
for text in new_data:
    lem = [i.lemma_ for i in text]
    new_data_lem.append(' '.join(lem))

count_vectorizer = CountVectorizer(new_data_lem, stop_words='english')

text_clf = Pipeline([('vect', count_vectorizer),
    ('tfidf', TfidfTransformer()),
    ('clf', MultinomialNB()),
])

text_clf.fit(new_data_lem, twenty_train.target)
predicted = text_clf.predict(docs_test)

print(metrics.classification_report(twenty_test.target, predicted,
    target_names=twenty_test.target_names))