import operator
import numpy as np
import time as t
import pandas as pd
import matplotlib.pyplot as plt


def make_matrix(n, num=10):
    return np.full((n, n), num)


def time_calc(n):
    a = make_matrix(n)
    begin_time = t.time()
    np.dot(a, a)
    spent_time = t.time() - begin_time
    return spent_time


def alg_complexity(n, t):
    alg_compl = [0]
    for i in range(1, len(n)):
        try:
            with np.errstate(divide='ignore'):
                new_compl = (np.log(t[i]/t[i-1]))/np.log(n[i]/n[i-1])
                if new_compl == -np.inf or new_compl == np.inf:
                    new_compl = 0
        except ZeroDivisionError:
            new_compl = 0
        alg_compl.append(new_compl)
    return alg_compl


def time_exp(n):
    all_time = []
    for i in range(100):
        time = time_calc(n)
        all_time.append(time)
    mean_time = np.mean(all_time)
    std_time = np.std(all_time)
    return mean_time, std_time


x = 25
gen_n = [x*i for i in range(2, 20)]
gen_times = [time_exp(n) for n in gen_n]
times = []
times_std = []
for t in gen_times:
    times.append(t[0])
    times_std.append(t[1])

alg_compl = alg_complexity(gen_n, times)

data = {'N': gen_n, 'T': times, 'n': alg_compl, 't_std': times_std}
df = pd.DataFrame(data)
print(df)


log_t = [0 if np.log(x) == -np.inf else np.log(x) for x in times]
log_n = [0 if np.log(x) == -np.inf else np.log(x) for x in alg_compl]


log_t_dict = {}
for i in range(len(log_t)):
    log_t_dict[log_n[i]] = log_t[i]

sorted_times = sorted(log_t_dict.items(), key=operator.itemgetter(1))
sorted_times_dict = {x[0]: x[1] for x in sorted_times}
p = np.polyfit(list(sorted_times_dict.keys()), list(sorted_times_dict.values()), 1)
yp = np.polyval(p, list(sorted_times_dict.keys()))
plt.title('Graph log(T)(log(N))')
plt.xlabel('log(N)')
plt.ylabel('log(T)')
plt.errorbar(log_n, log_t, times_std, None, fmt='.', label='Standard deviation')
plt.plot(list(sorted_times_dict.keys()), yp, 'r', label='log(T)(log(N))')
plt.legend(loc='best', shadow=True, fontsize='small')
plt.savefig('log_graph.png')
plt.savefig('log_graph.eps')
plt.show()


p = np.polyfit(gen_n, alg_compl, 1)
yp = np.polyval(p, gen_n)
plt.title('Graph n(N)')
plt.xlabel('N')
plt.ylabel('n')
plt.errorbar(gen_n, alg_compl, times_std, None, fmt='.', label='Standard deviation')
plt.plot(gen_n, yp, 'r', label='n(N)')
plt.legend(loc='best', shadow=True, fontsize='small')
plt.savefig('N_graph.png')
plt.savefig('N_graph.eps')
plt.show()
